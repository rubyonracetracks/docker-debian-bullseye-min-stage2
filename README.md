[![pipeline status](https://gitlab.com/rubyonracetracks/docker-debian-bullseye-min-stage2/badges/main/pipeline.svg)](https://gitlab.com/rubyonracetracks/docker-debian-bullseye-min-stage2/-/commits/main)

# Docker Debian Bullseye - Minimal - Stage 1

This repository is used for building the Stage 1 Minimal Debian Bullseye Docker image for [Ruby on Racetracks](https://www.rubyonracetracks.com/).

## Name of This Docker Image
[registry.gitlab.com/rubyonracetracks/docker-debian-bullseye-min-stage2](https://gitlab.com/rubyonracetracks/docker-debian-bullseye-min-stage2/container_registry)

## Upstream Docker Image
[registry.gitlab.com/rubyonracetracks/docker-debian-bullseye-min-stage1](https://gitlab.com/rubyonracetracks/docker-debian-bullseye-min-stage1/container_registry)

## What's Added
* Git
* Ansible
* Python
* SQLite, PostgreSQL, Redis, and other common external dependencies of Ruby on Rails apps
* Heroku
* NVM and Node.js

## Things NOT Included
Ruby version managers, such as RVM

## What's the Point?
* This Docker image is a building block for other Docker images for [Ruby on Racetracks](https://www.rubyonracetracks.com/).
* Software for managing Ruby versions (such as RVM) is NOT included in this Docker image.  (This is covered in downstream Docker images.)

## More Information
General information common to all Docker Debian build repositories is in the [FAQ](https://gitlab.com/rubyonracetracks/docker-common/blob/main/FAQ.md).
